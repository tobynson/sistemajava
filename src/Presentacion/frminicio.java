/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentacion;

import Files.fondo;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import javax.imageio.ImageIO;

/**
 *
 * @author lenovo
 */
public class frminicio extends javax.swing.JFrame {

    //agregado para el fondo ubicacion de la imagen
    public InputStream foto1 = this.getClass().getResourceAsStream("/Files/fondo1.jpg");

    /**
     * Creates new form frminicio
     */
    public frminicio() {
        initComponents();
        cargarImagen(escritorio,foto1);
        this.setExtendedState(frminicio.MAXIMIZED_BOTH);
        this.setTitle("Sistema de Reserva de Habitaciones y Gestion de Ventas ");
    }
    //AGREGADO PARA FONDO

    public void cargarImagen(javax.swing.JDesktopPane jDeskp,InputStream fileImagen) {
        try {
            BufferedImage image = ImageIO.read(fileImagen);
            jDeskp.setBorder(new fondo(image));
        } catch (Exception e) {
            System.out.println("Imagen no disponible");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        escritorio = new javax.swing.JDesktopPane();
        lblIdpersona = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        lblApaterno = new javax.swing.JLabel();
        lblAmaterno = new javax.swing.JLabel();
        lblAcceso = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        mnuSisreserva = new javax.swing.JMenu();
        mnuArchivo = new javax.swing.JMenu();
        cutMenuItem = new javax.swing.JMenuItem();
        copyMenuItem = new javax.swing.JMenuItem();
        mnuReservas = new javax.swing.JMenu();
        contentMenuItem = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        mnuConsultas = new javax.swing.JMenu();
        mnuConfiguraciones = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        mnuHerramientas = new javax.swing.JMenu();
        mnuAyuda = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        mnuSalir = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblIdpersona.setFont(new java.awt.Font("Arial Black", 1, 12)); // NOI18N
        lblIdpersona.setForeground(new java.awt.Color(255, 255, 255));
        lblIdpersona.setText("Id");
        escritorio.add(lblIdpersona);
        lblIdpersona.setBounds(20, 20, 50, 18);

        lblNombre.setFont(new java.awt.Font("Arial Black", 1, 12)); // NOI18N
        lblNombre.setForeground(new java.awt.Color(255, 255, 255));
        lblNombre.setText("Nombre");
        escritorio.add(lblNombre);
        lblNombre.setBounds(20, 40, 90, 18);

        lblApaterno.setFont(new java.awt.Font("Arial Black", 1, 12)); // NOI18N
        lblApaterno.setForeground(new java.awt.Color(255, 255, 255));
        lblApaterno.setText("Apaterno");
        escritorio.add(lblApaterno);
        lblApaterno.setBounds(20, 60, 130, 18);

        lblAmaterno.setFont(new java.awt.Font("Arial Black", 1, 12)); // NOI18N
        lblAmaterno.setForeground(new java.awt.Color(255, 255, 255));
        lblAmaterno.setText("Amaterno");
        escritorio.add(lblAmaterno);
        lblAmaterno.setBounds(20, 80, 130, 18);

        lblAcceso.setFont(new java.awt.Font("Arial Black", 1, 12)); // NOI18N
        lblAcceso.setForeground(new java.awt.Color(255, 255, 255));
        lblAcceso.setText("Acceso");
        escritorio.add(lblAcceso);
        lblAcceso.setBounds(20, 100, 130, 18);

        mnuSisreserva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Files/sistema32.png"))); // NOI18N
        mnuSisreserva.setMnemonic('f');
        mnuSisreserva.setText("Sistema");
        menuBar.add(mnuSisreserva);

        mnuArchivo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Files/archivo32.png"))); // NOI18N
        mnuArchivo.setMnemonic('e');
        mnuArchivo.setText("Archivo");

        cutMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.CTRL_MASK));
        cutMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Files/habitaciones24.png"))); // NOI18N
        cutMenuItem.setMnemonic('t');
        cutMenuItem.setText("Habitaciones");
        cutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cutMenuItemActionPerformed(evt);
            }
        });
        mnuArchivo.add(cutMenuItem);

        copyMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Files/productos24.png"))); // NOI18N
        copyMenuItem.setMnemonic('y');
        copyMenuItem.setText("Productos");
        copyMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyMenuItemActionPerformed(evt);
            }
        });
        mnuArchivo.add(copyMenuItem);

        menuBar.add(mnuArchivo);

        mnuReservas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Files/reservas32.png"))); // NOI18N
        mnuReservas.setMnemonic('h');
        mnuReservas.setText("Reservas");

        contentMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Files/reserva24.png"))); // NOI18N
        contentMenuItem.setMnemonic('c');
        contentMenuItem.setText("Reservas y Consumos");
        contentMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                contentMenuItemActionPerformed(evt);
            }
        });
        mnuReservas.add(contentMenuItem);

        aboutMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Files/clientes24.png"))); // NOI18N
        aboutMenuItem.setMnemonic('a');
        aboutMenuItem.setText("Clientes");
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        mnuReservas.add(aboutMenuItem);

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Files/pagos24.png"))); // NOI18N
        jMenuItem1.setText("Pagos");
        mnuReservas.add(jMenuItem1);

        menuBar.add(mnuReservas);

        mnuConsultas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Files/consultas32.png"))); // NOI18N
        mnuConsultas.setText("Consultas");
        menuBar.add(mnuConsultas);

        mnuConfiguraciones.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Files/configuraciones32.png"))); // NOI18N
        mnuConfiguraciones.setText("Configuracion");

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Files/usuarios24.png"))); // NOI18N
        jMenuItem2.setText("Usuarios y Accesos");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        mnuConfiguraciones.add(jMenuItem2);

        menuBar.add(mnuConfiguraciones);

        mnuHerramientas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Files/herramientas32.png"))); // NOI18N
        mnuHerramientas.setText("Herramientas");
        menuBar.add(mnuHerramientas);

        mnuAyuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Files/ayuda32.png"))); // NOI18N
        mnuAyuda.setText("Ayuda");

        jMenuItem3.setText("Acerca de...");
        mnuAyuda.add(jMenuItem3);

        jMenuItem4.setText("Ayuda");
        mnuAyuda.add(jMenuItem4);

        menuBar.add(mnuAyuda);

        mnuSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Files/salir32.png"))); // NOI18N
        mnuSalir.setText("Salir");
        mnuSalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mnuSalirMouseClicked(evt);
            }
        });
        menuBar.add(mnuSalir);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(escritorio, javax.swing.GroupLayout.DEFAULT_SIZE, 775, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(escritorio, javax.swing.GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cutMenuItemActionPerformed
        // TODO add your handling code here:
        frmhabitacion frm=new frmhabitacion();
        escritorio.add(frm);
        frm.toFront();
        frm.setVisible(true);
    }//GEN-LAST:event_cutMenuItemActionPerformed

    private void copyMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyMenuItemActionPerformed
        // TODO add your handling code here:
        frmproducto frm=new frmproducto();
        escritorio.add(frm);
        frm.toFront();
        frm.setVisible(true);
    }//GEN-LAST:event_copyMenuItemActionPerformed

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
        // TODO add your handling code here:
        frmcliente frm=new frmcliente();
        escritorio.add(frm);
        frm.toFront();
        frm.setVisible(true);
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        frmtrabajador frm=new frmtrabajador();
        escritorio.add(frm);
        frm.toFront();
        frm.setVisible(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void mnuSalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuSalirMouseClicked
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_mnuSalirMouseClicked

    private void contentMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contentMenuItemActionPerformed
        // TODO add your handling code here:
        frmreserva frm=new frmreserva();
        escritorio.add(frm);
        frm.toFront();
        frm.setVisible(true);
        //lbl almacena el cod de quien inicio sesion
        frmreserva.txtIdtrabajador.setText(lblIdpersona.getText());
        frmreserva.txtTrabajador.setText(lblNombre.getText()+" "+lblApaterno.getText());    
        frmreserva.idusuario=Integer.parseInt(lblIdpersona.getText());//usuario q ingreso a sesion de sistema
    }//GEN-LAST:event_contentMenuItemActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frminicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frminicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frminicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frminicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frminicio().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JMenuItem contentMenuItem;
    private javax.swing.JMenuItem copyMenuItem;
    private javax.swing.JMenuItem cutMenuItem;
    public static javax.swing.JDesktopPane escritorio;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    public static javax.swing.JLabel lblAcceso;
    public static javax.swing.JLabel lblAmaterno;
    public static javax.swing.JLabel lblApaterno;
    public static javax.swing.JLabel lblIdpersona;
    public static javax.swing.JLabel lblNombre;
    private javax.swing.JMenuBar menuBar;
    public static javax.swing.JMenu mnuArchivo;
    private javax.swing.JMenu mnuAyuda;
    public static javax.swing.JMenu mnuConfiguraciones;
    private javax.swing.JMenu mnuConsultas;
    private javax.swing.JMenu mnuHerramientas;
    private javax.swing.JMenu mnuReservas;
    private javax.swing.JMenu mnuSalir;
    private javax.swing.JMenu mnuSisreserva;
    // End of variables declaration//GEN-END:variables

}
