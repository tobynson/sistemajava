/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentacion;

import Datos.vreserva;
import Logica.freserva;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.sql.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author lenovo
 */
public class frmreserva extends javax.swing.JInternalFrame {

    /**
     * Creates new form frmreserva
     */
    public frmreserva() {
        initComponents();
        this.setClosable(true);//muestra la x        
        this.setMaximizable(true);//maximiza
        this.setIconifiable(true);//minimizar
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);//accion de la x
        
        mostrar("");
        inhabilitar();
    }
    private String accion="Guardar";
    public static int idusuario;//usuario q ingreso a sesion de sistema
    public void ocultar_columnas(){
        tablaListado.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaListado.getColumnModel().getColumn(0).setMinWidth(0);
        tablaListado.getColumnModel().getColumn(0).setPreferredWidth(0);
        
        tablaListado.getColumnModel().getColumn(1).setMaxWidth(0);
        tablaListado.getColumnModel().getColumn(1).setMinWidth(0);
        tablaListado.getColumnModel().getColumn(1).setPreferredWidth(0);
        
        tablaListado.getColumnModel().getColumn(3).setMaxWidth(0);
        tablaListado.getColumnModel().getColumn(3).setMinWidth(0);
        tablaListado.getColumnModel().getColumn(3).setPreferredWidth(0);
        
        tablaListado.getColumnModel().getColumn(5).setMaxWidth(0);
        tablaListado.getColumnModel().getColumn(5).setMinWidth(0);
        tablaListado.getColumnModel().getColumn(5).setPreferredWidth(0);
    }
    public void inhabilitar(){
        txtIdreserva.setVisible(false);
        txtIdhabitacion.setEnabled(false);
        txtIdcliente.setEnabled(false);
        txtIdtrabajador.setEnabled(false);
        txtNumero_habitacion.setEnabled(false);
        txtCliente.setEnabled(false); 
        txtTrabajador.setEnabled(false);
        cmbReserva.setEnabled(false);
        cmbEstado.setEnabled(false);
        txtCosto.setEnabled(false);
        dcFecha_reserva.setEnabled(false);
        dcFecha_ingreso.setEnabled(false);
        dcFecha_salidad.setEnabled(false);
        btnBuscar_habitacion.setEnabled(false);
        btnBuscar_cliente.setEnabled(false);
                      
        btnGuardar.setEnabled(false);
        btnCancelar.setEnabled(false);
        btnEliminar.setEnabled(false);
        txtIdreserva.setText("");
        txtIdhabitacion.setText("");
        txtIdcliente.setText("");
        //txtIdtrabajador.setText("");
        txtNumero_habitacion.setText("");
        txtCliente.setText("");
        //txtTrabajador.setText("");
        cmbReserva.setSelectedIndex(-1);//resetea jcombo
        cmbEstado.setSelectedIndex(-1);
        txtCosto.setText("");
         //Calendar c2 = new GregorianCalendar();//fecha actual
        dcFecha_reserva.setCalendar(null);
        dcFecha_ingreso.setCalendar(null);
        dcFecha_salidad.setCalendar(null);
        txtCosto.requestFocus();//poner en foco
    }
    public void habilitar(){
        txtIdreserva.setVisible(true);
        txtIdhabitacion.setEnabled(true);
        txtIdcliente.setEnabled(true);
        txtIdtrabajador.setEnabled(false);
        txtNumero_habitacion.setEnabled(true);
        txtCliente.setEnabled(true); 
        txtTrabajador.setEnabled(false);
        cmbReserva.setEnabled(true);
        cmbEstado.setEnabled(true);
        txtCosto.setEnabled(true);
        dcFecha_reserva.setEnabled(true);
        dcFecha_ingreso.setEnabled(true);
        dcFecha_salidad.setEnabled(true);
        btnBuscar_habitacion.setEnabled(true);
        btnBuscar_cliente.setEnabled(true);
                      
        btnGuardar.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnEliminar.setEnabled(true);
        txtIdreserva.setText("");
        txtIdhabitacion.setText("");
        txtIdcliente.setText("");
        //txtIdtrabajador.setText("");
        txtNumero_habitacion.setText("");
        txtCliente.setText("");
        //txtTrabajador.setText("");
        cmbReserva.setSelectedIndex(-1);//resetea jcombo
        cmbEstado.setSelectedIndex(-1);
        txtCosto.setText("");
        
        dcFecha_reserva.setCalendar(null);
        dcFecha_ingreso.setCalendar(null);
        dcFecha_salidad.setCalendar(null);
        txtCosto.requestFocus();//poner en foco
    }
    public void mostrar(String buscar){
        try {
            DefaultTableModel modelo;
            freserva func =new freserva();
            modelo=func.mostrar(buscar);            
            tablaListado.setModel(modelo);
            ocultar_columnas();
            
            lblTotal_registros.setText("total de registros:"+Integer.toString(func.totalRegistros));
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabPane = new javax.swing.JTabbedPane();
        panelListado = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tablaListado = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        txtBuscar = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        lblTotal_registros = new javax.swing.JLabel();
        btnVer_consumo = new javax.swing.JButton();
        panelRegistro = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtIdreserva = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtNumero_habitacion = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        txtIdhabitacion = new javax.swing.JTextField();
        txtIdcliente = new javax.swing.JTextField();
        txtCliente = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtIdtrabajador = new javax.swing.JTextField();
        txtTrabajador = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        cmbReserva = new javax.swing.JComboBox<>();
        dcFecha_reserva = new com.toedter.calendar.JDateChooser();
        dcFecha_ingreso = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        dcFecha_salidad = new com.toedter.calendar.JDateChooser();
        jLabel10 = new javax.swing.JLabel();
        txtCosto = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        cmbEstado = new javax.swing.JComboBox<>();
        btnBuscar_habitacion = new javax.swing.JButton();
        btnBuscar_cliente = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        tabPane.setBackground(new java.awt.Color(102, 153, 0));
        tabPane.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        panelListado.setBackground(new java.awt.Color(153, 153, 0));

        tablaListado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaListado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaListadoMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tablaListado);

        jLabel9.setText("Buscar");

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        lblTotal_registros.setText("Registros");

        btnVer_consumo.setText("Ver Consumo...");
        btnVer_consumo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVer_consumoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelListadoLayout = new javax.swing.GroupLayout(panelListado);
        panelListado.setLayout(panelListadoLayout);
        panelListadoLayout.setHorizontalGroup(
            panelListadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelListadoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelListadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelListadoLayout.createSequentialGroup()
                        .addComponent(btnVer_consumo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTotal_registros)
                        .addGap(93, 93, 93))
                    .addGroup(panelListadoLayout.createSequentialGroup()
                        .addGroup(panelListadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 468, Short.MAX_VALUE)
                            .addGroup(panelListadoLayout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnBuscar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnEliminar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSalir)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        panelListadoLayout.setVerticalGroup(
            panelListadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelListadoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelListadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscar)
                    .addComponent(btnEliminar)
                    .addComponent(btnSalir)
                    .addComponent(txtBuscar)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelListadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTotal_registros)
                    .addComponent(btnVer_consumo))
                .addContainerGap())
        );

        tabPane.addTab("Listado", panelListado);

        panelRegistro.setBackground(new java.awt.Color(153, 153, 255));
        panelRegistro.setBorder(javax.swing.BorderFactory.createTitledBorder("Registro de Reservas"));

        jLabel1.setText("Habitacion");

        jLabel2.setText("Codigo");

        txtNumero_habitacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumero_habitacionActionPerformed(evt);
            }
        });

        jLabel4.setText("Cliente");

        jLabel6.setText("F. Reserva");

        jLabel8.setText("F. Ingreso");

        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        txtIdhabitacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdhabitacionActionPerformed(evt);
            }
        });

        txtIdcliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdclienteActionPerformed(evt);
            }
        });

        txtCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtClienteActionPerformed(evt);
            }
        });

        jLabel5.setText("Trabajador");

        txtIdtrabajador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdtrabajadorActionPerformed(evt);
            }
        });

        txtTrabajador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTrabajadorActionPerformed(evt);
            }
        });

        jLabel3.setText("T. Reserva");

        cmbReserva.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Reserva", "Alquiler" }));

        dcFecha_reserva.setDateFormatString("yyyy-mm-dd");

        dcFecha_ingreso.setDateFormatString("yyyy-mm-dd");

        jLabel7.setText("F. Salidad");

        dcFecha_salidad.setDateFormatString("yyyy-mm-dd");

        jLabel10.setText("Costo");

        jLabel11.setText("Estado");

        cmbEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Alquiler", "Pagado", "Anulado" }));

        btnBuscar_habitacion.setText("...");
        btnBuscar_habitacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscar_habitacionActionPerformed(evt);
            }
        });

        btnBuscar_cliente.setText("...");
        btnBuscar_cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscar_clienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelRegistroLayout = new javax.swing.GroupLayout(panelRegistro);
        panelRegistro.setLayout(panelRegistroLayout);
        panelRegistroLayout.setHorizontalGroup(
            panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRegistroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelRegistroLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnNuevo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnGuardar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCancelar)
                        .addGap(100, 100, 100))
                    .addGroup(panelRegistroLayout.createSequentialGroup()
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelRegistroLayout.createSequentialGroup()
                                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelRegistroLayout.createSequentialGroup()
                                        .addComponent(txtIdreserva, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 143, Short.MAX_VALUE))
                                    .addGroup(panelRegistroLayout.createSequentialGroup()
                                        .addComponent(txtIdhabitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtNumero_habitacion)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnBuscar_habitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(20, 20, 20)))
                                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel6)
                                    .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel7)
                                        .addComponent(jLabel8)
                                        .addComponent(jLabel10)
                                        .addComponent(jLabel11)))
                                .addGap(35, 35, 35))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelRegistroLayout.createSequentialGroup()
                                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelRegistroLayout.createSequentialGroup()
                                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addGroup(panelRegistroLayout.createSequentialGroup()
                                                .addComponent(txtIdcliente, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtCliente))
                                            .addComponent(cmbReserva, javax.swing.GroupLayout.Alignment.LEADING, 0, 118, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnBuscar_cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelRegistroLayout.createSequentialGroup()
                                        .addComponent(txtIdtrabajador, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtTrabajador, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtCosto, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(dcFecha_salidad, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                            .addComponent(dcFecha_ingreso, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dcFecha_reserva, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbEstado, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(27, 27, 27))))
        );
        panelRegistroLayout.setVerticalGroup(
            panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRegistroLayout.createSequentialGroup()
                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelRegistroLayout.createSequentialGroup()
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtIdreserva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel2)
                                .addComponent(jLabel6))
                            .addComponent(dcFecha_reserva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1)
                                .addComponent(txtIdhabitacion)
                                .addComponent(txtNumero_habitacion)
                                .addComponent(btnBuscar_habitacion))
                            .addGroup(panelRegistroLayout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(panelRegistroLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(dcFecha_ingreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelRegistroLayout.createSequentialGroup()
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtIdcliente)
                            .addComponent(txtCliente)
                            .addComponent(btnBuscar_cliente)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtIdtrabajador)
                                .addComponent(txtTrabajador))
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(cmbReserva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(52, 52, 52))
                    .addGroup(panelRegistroLayout.createSequentialGroup()
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dcFecha_salidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtCosto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(cmbEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(panelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNuevo)
                    .addComponent(btnGuardar)
                    .addComponent(btnCancelar))
                .addContainerGap())
        );

        tabPane.addTab("Registro", panelRegistro);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabPane, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabPane, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablaListadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaListadoMouseClicked
//        try {
            // al hacer click en un registro
            btnGuardar.setText("Editar");
            habilitar();
            btnEliminar.setEnabled(true);
            accion="Editar";
            
            int fila=tablaListado.rowAtPoint(evt.getPoint());//la fila donde hice click
            txtIdreserva.setText(tablaListado.getValueAt(fila,0).toString());
            txtIdhabitacion.setText(tablaListado.getValueAt(fila,1).toString());
            txtNumero_habitacion.setText(tablaListado.getValueAt(fila,2).toString());
            txtIdcliente.setText(tablaListado.getValueAt(fila,3).toString());
            txtCliente.setText(tablaListado.getValueAt(fila,4).toString());
            txtIdtrabajador.setText(tablaListado.getValueAt(fila,5).toString());
            txtTrabajador.setText(tablaListado.getValueAt(fila,6).toString());
            cmbReserva.setSelectedItem(tablaListado.getValueAt(fila,7).toString());
            
           // SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");            
            dcFecha_reserva.setDate(Date.valueOf(tablaListado.getValueAt(fila,8).toString()));               
            dcFecha_ingreso.setDate(Date.valueOf(tablaListado.getValueAt(fila,9).toString()));
            dcFecha_salidad.setDate(Date.valueOf(tablaListado.getValueAt(fila,10).toString()));
            
            txtCosto.setText(tablaListado.getValueAt(fila,11).toString());
            cmbEstado.setSelectedItem(tablaListado.getValueAt(fila,12).toString());
            
            tabPane.setSelectedIndex(1);//me cambia de pestaña al guardar
//        } catch (ParseException ex) {
//            Logger.getLogger(frmreserva.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }//GEN-LAST:event_tablaListadoMouseClicked

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        // TODO add your handling code here:
        mostrar(txtBuscar.getText());
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        // TODO add your handling code here:
        if (!txtIdreserva.getText().equals("")) {
            int confirmacion=JOptionPane.showConfirmDialog(rootPane,"Estas seguro de eliminar la reserva","Confirmar",JOptionPane.YES_NO_OPTION);
            if (JOptionPane.YES_OPTION==confirmacion) {

                vreserva dts=new vreserva();
                freserva func =new freserva();

                dts.setIdreserva(Integer.parseInt(txtIdreserva.getText()));
                func.eliminar(dts);
                mostrar("");
                inhabilitar();
            }
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void txtNumero_habitacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumero_habitacionActionPerformed
        // TODO add your handling code here:
        txtNumero_habitacion.transferFocus();
    }//GEN-LAST:event_txtNumero_habitacionActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        habilitar();
        btnGuardar.setText("Guardar");
        accion="Guardar";
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        if (txtNumero_habitacion.getText().length()==0) {
            JOptionPane.showConfirmDialog(rootPane,"Debes seleccionar un numero de habitacion");
            txtNumero_habitacion.requestFocus();
            return;//si deja en blanco retorna
        }
        if (txtCliente.getText().length()==0) {
            JOptionPane.showConfirmDialog(rootPane,"Debes seleccionar un cliente");
            txtCliente.requestFocus();
            return;//si deja en blanco retorna
        }
        if (txtCosto.getText().length()==0) {
            JOptionPane.showConfirmDialog(rootPane,"Debes ingresar el costo de la habitacion");
            txtCosto.requestFocus();
            return;//si deja en blanco retorna
        }
        if (cmbReserva.getSelectedIndex()==-1) {
            JOptionPane.showConfirmDialog(rootPane,"Debes seleccionar un tipo de reserva");
            cmbReserva.requestFocus();
            return;//si deja en blanco retorna
        }
        if (cmbEstado.getSelectedIndex()==-1) {
            JOptionPane.showConfirmDialog(rootPane,"Debes seleccionar el estado de la reserva");
            cmbEstado.requestFocus();
            return;//si deja en blanco retorna
        }
        if (dcFecha_reserva.getDate()==null) {
            JOptionPane.showConfirmDialog(rootPane,"Debes seleccionar la fecha de reserva");
            dcFecha_reserva.requestFocus();
            return;//si deja en blanco retorna
        }
        if (dcFecha_ingreso.getDate()==null) {
            JOptionPane.showConfirmDialog(rootPane,"Debes seleccionar la fecha de ingreso");
            dcFecha_ingreso.requestFocus();
            return;//si deja en blanco retorna
        }
        if (dcFecha_salidad.getDate()==null) {
            JOptionPane.showConfirmDialog(rootPane,"Debes seleccionar la fecha de salidad");
            dcFecha_salidad.requestFocus();
            return;//si deja en blanco retorna
        }

        vreserva dts=new vreserva();
        freserva func =new freserva();

        //envio datos de los controles a los metodos set
        dts.setIdhabitacion(Integer.parseInt(txtIdhabitacion.getText()));
        dts.setIdcliente(Integer.parseInt(txtIdcliente.getText()));
        dts.setIdtrabajador(idusuario);
        dts.setTrabajador(txtTrabajador.getText());
        //para enviar un combo debeo seleccionar primero el indice
        int seleccionado=cmbReserva.getSelectedIndex();
        dts.setTipo_reserva((String)cmbReserva.getItemAt(seleccionado));
        
        seleccionado=cmbEstado.getSelectedIndex();
        dts.setEstado((String)cmbEstado.getItemAt(seleccionado));

        Calendar cal;
        int dia,mes,año;
        
        cal=dcFecha_reserva.getCalendar();        
        dia=cal.get(Calendar.DAY_OF_YEAR);
        mes=cal.get(Calendar.MONTH);
        año=cal.get(Calendar.YEAR)-1900;//para obyener formato correcto        
        dts.setFecha_reserva(new Date(año,mes,dia));
        
        cal=dcFecha_ingreso.getCalendar();        
        dia=cal.get(Calendar.DAY_OF_YEAR);
        mes=cal.get(Calendar.MONTH);
        año=cal.get(Calendar.YEAR)-1900;//para obyener formato correcto        
        dts.setFecha_ingreso(new Date(año,mes,dia));
        
        cal=dcFecha_salidad.getCalendar();        
        dia=cal.get(Calendar.DAY_OF_YEAR);
        mes=cal.get(Calendar.MONTH);
        año=cal.get(Calendar.YEAR)-1900;//para obyener formato correcto        
        dts.setFecha_salidad(new Date(año,mes,dia));
        
        dts.setCosto_total(Double.parseDouble(txtCosto.getText()));
        
        //verificar si quiero guardar o editar
        if (accion.equals("Guardar")) {
            if (func.insertar(dts)) {
                JOptionPane.showMessageDialog(rootPane, "la reserva fue fuardada correctamente");
                mostrar("");
                inhabilitar();
                tabPane.setSelectedIndex(0);//me cambia de pestaña al guardar
            }
        } else if(accion.equals("Editar")){
            dts.setIdreserva(Integer.parseInt(txtIdreserva.getText()));
            dts.setIdtrabajador(Integer.parseInt(txtIdtrabajador.getText()));//q trabajador edito
            if (func.editar(dts)) {
                JOptionPane.showMessageDialog(rootPane, "la reserva fue actualizada correctamente");
                mostrar("");
                inhabilitar();
                tabPane.setSelectedIndex(0);//me cambia de pestaña al guardar
            }
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtIdhabitacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdhabitacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdhabitacionActionPerformed

    private void txtIdclienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdclienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdclienteActionPerformed

    private void txtClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtClienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtClienteActionPerformed

    private void txtIdtrabajadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdtrabajadorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdtrabajadorActionPerformed

    private void txtTrabajadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTrabajadorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTrabajadorActionPerformed
    //llamada al formulario busar habitacion
    private void btnBuscar_habitacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscar_habitacionActionPerformed
        // TODO add your handling code here:
        frmvista_habitacion frm =new frmvista_habitacion();
        frm.toFront();
        frm.setVisible(true);
    }//GEN-LAST:event_btnBuscar_habitacionActionPerformed
    //llamada al formulario busar cliente
    private void btnBuscar_clienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscar_clienteActionPerformed
        // TODO add your handling code here:
        frmvista_cliente frm =new frmvista_cliente();
        frm.toFront();
        frm.setVisible(true);
    }//GEN-LAST:event_btnBuscar_clienteActionPerformed

    private void btnVer_consumoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVer_consumoActionPerformed
        // TODO add your handling code here:
        int fila=tablaListado.getSelectedRow();
        
        frmconsumo.idreserva=tablaListado.getValueAt(fila,0).toString();
        frmconsumo.cliente=tablaListado.getValueAt(fila,4).toString();
        
        //muestro frmconsumo desde inicio hacer el desktoppane public static
        frmconsumo frm=new frmconsumo();
        frminicio.escritorio.add(frm);
        frm.toFront();
        frm.setVisible(true);
    }//GEN-LAST:event_btnVer_consumoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmreserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmreserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmreserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmreserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmreserva().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnBuscar_cliente;
    private javax.swing.JButton btnBuscar_habitacion;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btnVer_consumo;
    private javax.swing.JComboBox<String> cmbEstado;
    private javax.swing.JComboBox<String> cmbReserva;
    private com.toedter.calendar.JDateChooser dcFecha_ingreso;
    private com.toedter.calendar.JDateChooser dcFecha_reserva;
    private com.toedter.calendar.JDateChooser dcFecha_salidad;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lblTotal_registros;
    private javax.swing.JPanel panelListado;
    private javax.swing.JPanel panelRegistro;
    private javax.swing.JTabbedPane tabPane;
    private javax.swing.JTable tablaListado;
    private javax.swing.JTextField txtBuscar;
    public static javax.swing.JTextField txtCliente;
    private javax.swing.JTextField txtCosto;
    public static javax.swing.JTextField txtIdcliente;
    public static javax.swing.JTextField txtIdhabitacion;
    private javax.swing.JTextField txtIdreserva;
    public static javax.swing.JTextField txtIdtrabajador;
    public static javax.swing.JTextField txtNumero_habitacion;
    public static javax.swing.JTextField txtTrabajador;
    // End of variables declaration//GEN-END:variables
}
