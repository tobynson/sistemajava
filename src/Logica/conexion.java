package Logica;

import java.sql.*;//para no hacer muchas importaciones
import javax.swing.JOptionPane;

public class conexion {
    public String db="dbreserva";
    public String url="jdbc:mysql://localhost/"+db;
    public String driver = "com.mysql.jdbc.Driver";
    public String user="root";
    public String pass="";

    public conexion() {
    }
    public Connection Conectar(){
        Connection  link=null;
        try {
            Class.forName(driver);
            link=DriverManager.getConnection(this.url,this.user,this.pass);
        } catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return link;
    }
}
