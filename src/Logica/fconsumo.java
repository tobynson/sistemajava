package Logica;

import Datos.vconsumo;
import java.sql.*;//para no hacer muchas importaciones
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class fconsumo {
    //instanciamos la clase conexion y accedo a su metodp conectar
    private conexion mysql = new conexion();
    private Connection cn =  mysql.Conectar();
    private String sql="";
    public Integer totalRegistros;
    public Double totalConsumo;
    
    //mostrar
    public DefaultTableModel mostrar(String buscar){
        DefaultTableModel modelo;
        
        String[]titulos={"Id","Idreserva","Idproducto","Producto","Cantidad","Precio Venta","Estado"};
        String[]registros=new String[7];
        totalRegistros=0;
        totalConsumo=0.0;
        
        modelo=new DefaultTableModel(null,titulos);
        sql="select c.idconsumo,c.idreserva,c.idproducto,p.nombre,c.cantidad,c.precio_venta,c.estado from consumo c "+
                " inner join producto p on c.idproducto=p.idproducto"+
                " where c.idreserva='"+buscar+"' order by idconsumo desc";
        try {
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while (rs.next()) {      //de acuerdo al orden de la tabla          
                registros[0]=rs.getString("idconsumo");
                registros[1]=rs.getString("idreserva");
                registros[2]=rs.getString("idproducto");
                registros[3]=rs.getString("nombre");
                registros[4]=rs.getString("cantidad");
                registros[5]=rs.getString("precio_venta");
                registros[6]=rs.getString("estado");
                                
                totalRegistros=totalRegistros+1;
                totalConsumo=totalConsumo+(rs.getDouble("cantidad")*rs.getDouble("precio_venta"));
                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return null;
        }
    }
    //insertar
    public boolean insertar(vconsumo dts){
        sql="insert into consumo(idreserva,idproducto,cantidad,precio_venta,estado) values(?,?,?,?,?)";
        try {//de acuerdo a los ?
            PreparedStatement pst=cn.prepareStatement(sql);
            pst.setInt(1,dts.getIdreserva());
            pst.setInt(2,dts.getIdproducto());
            pst.setDouble(3,dts.getCantidad());
            pst.setDouble(4,dts.getPrecio_venta());
            pst.setString(5,dts.getEstado());
                      
            int n=pst.executeUpdate();
            if (n!=0) {
                return true;
            }else{
                return false;                
            }
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    //editar
    public boolean editar(vconsumo dts){
        sql="update consumo set idreserva=?,idproducto=?,cantidad=?,precio_venta=?,estado=? where idconsumo=?";
        try {            
            PreparedStatement pst=cn.prepareStatement(sql);
            
            pst.setInt(1,dts.getIdreserva());
            pst.setInt(2,dts.getIdproducto());
            pst.setDouble(3,dts.getCantidad());
            pst.setDouble(4,dts.getPrecio_venta());
            pst.setString(5,dts.getEstado());
            
            pst.setInt(6, dts.getIdconsumo());//orden del signo en la sentencia sql
            
            int n=pst.executeUpdate();
            if (n!=0) {
                return true;
            }else{
                return false;
            }
            
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    //eliminar
    public boolean eliminar(vconsumo dts){
        sql="delete from consumo where idconsumo=?";
        try {
            PreparedStatement pst=cn.prepareStatement(sql);
            pst.setInt(1, dts.getIdconsumo());         
            int n=pst.executeUpdate();
            if (n!=0) {
                return true;
            }else{
                return false;
            }
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    
}
