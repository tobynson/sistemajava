package Logica;

import Datos.vcliente;
import Datos.vpersona;
import java.sql.*;//para no hacer muchas importaciones
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class fcliente {
     //instanciamos la clase conexion y accedo a su metodp conectar
    private conexion mysql = new conexion();
    private Connection cn =  mysql.Conectar();
    private String sql="";
    private String sql2="";
    public Integer totalRegistros;
    
    //mostrar
    public DefaultTableModel mostrar(String buscar){
        DefaultTableModel modelo;
        
        String[]titulos={"Id","Nombre","Apaterno","Amaterno","Tipo","Numero","Direccion","Telefono","Email","Codigo"};
        String[]registros=new String[10];
        totalRegistros=0;
        
        modelo=new DefaultTableModel(null,titulos);
        sql="select p.idpersona,p.nombre,p.apaterno,p.amaterno,p.tipo_documento,p.num_documento,"+
                "p.direccion,p.telefono,p.email,c.codigo_cliente "+
                "from persona p inner join cliente c on  p.idpersona=c.idpersona where num_documento like '%"+buscar+"%' order by idpersona";
        try {
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while (rs.next()) {      //de acuerdo al orden de la tabla          
                registros[0]=rs.getString("idpersona");
                registros[1]=rs.getString("nombre");
                registros[2]=rs.getString("apaterno");
                registros[3]=rs.getString("amaterno");
                registros[4]=rs.getString("tipo_documento");
                registros[5]=rs.getString("num_documento");
                registros[6]=rs.getString("direccion");
                registros[7]=rs.getString("telefono");
                registros[8]=rs.getString("email");
                registros[9]=rs.getString("codigo_cliente");
                                
                totalRegistros=totalRegistros+1;
                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return null;
        }
    }
    //insertar
    public boolean insertar(vcliente dts){
        sql="insert into persona(nombre,apaterno,amaterno,tipo_documento,num_documento,direccion,telefono,email) values(?,?,?,?,?,?,?,?)";
        sql2="insert into cliente(idpersona,codigo_cliente) values((select idpersona from persona order by idpersona desc limit 1),?)";
        try {//de acuerdo a los ?
            PreparedStatement pst=cn.prepareStatement(sql);            
            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getAmaterno());
            pst.setString(3, dts.getApaterno());
            pst.setString(4, dts.getTipo_documento());
            pst.setString(5, dts.getNum_documento());
            pst.setString(6, dts.getDireccion());
            pst.setString(7, dts.getTelefono());
            pst.setString(8, dts.getEmail());
            
            PreparedStatement pst2=cn.prepareStatement(sql2);            
            pst2.setString(1, dts.getCodigo_cliente());
                      
            int n=pst.executeUpdate();
            if (n!=0) {//si puedo insertar una persona inserto un cliente
                int n2=pst2.executeUpdate();
                if (n2!=0) {
                   return true; 
                }else{
                    return false;
                }
            }else{
                return false;                
            }
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    //editar
    public boolean editar(vcliente dts){
        sql="update persona set nombre=?,apaterno=?,amaterno=?,tipo_documento=?,num_documento=?,direccion=?,telefono=?,email=? where idpersona=?";
        sql2="update cliente set codigo_cliente=? where idpersona=?";
       
        try {//de acuerdo a los ?
            PreparedStatement pst=cn.prepareStatement(sql);            
            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getAmaterno());
            pst.setString(3, dts.getApaterno());
            pst.setString(4, dts.getTipo_documento());
            pst.setString(5, dts.getNum_documento());
            pst.setString(6, dts.getDireccion());
            pst.setString(7, dts.getTelefono());
            pst.setString(8, dts.getEmail());
            pst.setInt(9, dts.getIdpersona());
            
            PreparedStatement pst2=cn.prepareStatement(sql2);            
            pst2.setString(1, dts.getCodigo_cliente());
            pst2.setInt(2, dts.getIdpersona());
                      
            int n=pst.executeUpdate();
            if (n!=0) {//si puedo insertar una persona inserto un cliente
                int n2=pst2.executeUpdate();
                if (n2!=0) {
                   return true; 
                }else{
                    return false;
                }
            }else{
                return false;                
            }                   
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    //eliminar
    public boolean eliminar(vpersona dts){
        sql="delete from cliente where idpersona=?";
        sql2="delete from persona where idpersona=?";
        
        try {//de acuerdo a los ?
            PreparedStatement pst=cn.prepareStatement(sql);                   
            pst.setInt(1, dts.getIdpersona());
            
            PreparedStatement pst2=cn.prepareStatement(sql2);                        
            pst2.setInt(1, dts.getIdpersona());
                      
            int n=pst.executeUpdate();
            if (n!=0) {//si puedo insertar una persona inserto un cliente
                int n2=pst2.executeUpdate();
                if (n2!=0) {
                   return true; 
                }else{
                    return false;
                }
            }else{
                return false;                
            }                   
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
}
