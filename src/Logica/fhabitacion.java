package Logica;

import Datos.vhabitacion;
import java.sql.*;//para no hacer muchas importaciones
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class fhabitacion {
    //instanciamos la clase conexion y accedo a su metodp conectar
    private conexion mysql = new conexion();
    private Connection cn =  mysql.Conectar();
    private String sql="";
    public Integer totalRegistros;
    
    //mostrar
    public DefaultTableModel mostrar(String buscar){
        DefaultTableModel modelo;
        
        String[]titulos={"Id","Numero","Piso","Descripcion","Caracteristicas","Precio","Estado","Tipo habitacion"};
        String[]registros=new String[8];
        totalRegistros=0;
        
        modelo=new DefaultTableModel(null,titulos);
        sql="select * from habitacion where piso like '%"+buscar+"%' order by idhabitacion";
        try {
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while (rs.next()) {                
                registros[0]=rs.getString("idhabitacion");
                registros[1]=rs.getString("numero");
                registros[2]=rs.getString("piso");
                registros[3]=rs.getString("descripcion");
                registros[4]=rs.getString("caracteristicas");
                registros[5]=rs.getString("precio_diario");
                registros[6]=rs.getString("estado");
                registros[7]=rs.getString("tipo_habitacion");
                
                totalRegistros=totalRegistros+1;
                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return null;
        }
    }
    //insertar
    public boolean insertar(vhabitacion dts){
        sql="insert into habitacion(numero,piso,descripcion,caracteristicas,precio_diario,estado,tipo_habitacion) values(?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst=cn.prepareStatement(sql);
            pst.setString(1, dts.getNumero());
            pst.setString(2, dts.getPiso());
            pst.setString(3, dts.getDescripcion());
            pst.setString(4, dts.getCaracteristicas());
            pst.setDouble(5, dts.getPrecio_diario());
            pst.setString(6, dts.getEstado());
            pst.setString(7, dts.getTipo_habitacion());
            
            int n=pst.executeUpdate();
            if (n!=0) {
                return true;
            }else{
                return false;                
            }
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    //editar
    public boolean editar(vhabitacion dts){
        sql="update habitacion set numero=?,piso=?,descripcion=?,caracteristicas=?,precio_diario=?,estado=?,tipo_habitacion=? where idhabitacion=?";
        try {            
            PreparedStatement pst=cn.prepareStatement(sql);
            pst.setInt(8, dts.getIdhabitacion());//orden del signo en la sentencia sql
            pst.setString(1, dts.getNumero());
            pst.setString(2, dts.getPiso());
            pst.setString(3, dts.getDescripcion());
            pst.setString(4, dts.getCaracteristicas());
            pst.setDouble(5, dts.getPrecio_diario());
            pst.setString(6, dts.getEstado());
            pst.setString(7, dts.getTipo_habitacion());
            
            int n=pst.executeUpdate();
            if (n!=0) {
                return true;
            }else{
                return false;
            }
            
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    //eliminar
    public boolean eliminar(vhabitacion dts){
        sql="delete from habitacion where idhabitacion=?";
        try {
            PreparedStatement pst=cn.prepareStatement(sql);
            pst.setInt(1, dts.getIdhabitacion());         
            int n=pst.executeUpdate();
            if (n!=0) {
                return true;
            }else{
                return false;
            }
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    //mostrar vista habitacion
    public DefaultTableModel mostrar_vista_habitacion(String buscar){
        DefaultTableModel modelo;
        
        String[]titulos={"Id","Numero","Piso","Descripcion","Caracteristicas","Precio","Estado","Tipo habitacion"};
        String[]registros=new String[8];
        totalRegistros=0;
        
        modelo=new DefaultTableModel(null,titulos);
        sql="select * from habitacion where piso like '%"+buscar+"%' and estado='Disponible' order by idhabitacion";
        try {
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while (rs.next()) {                
                registros[0]=rs.getString("idhabitacion");
                registros[1]=rs.getString("numero");
                registros[2]=rs.getString("piso");
                registros[3]=rs.getString("descripcion");
                registros[4]=rs.getString("caracteristicas");
                registros[5]=rs.getString("precio_diario");
                registros[6]=rs.getString("estado");
                registros[7]=rs.getString("tipo_habitacion");
                
                totalRegistros=totalRegistros+1;
                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return null;
        }
    }
}
