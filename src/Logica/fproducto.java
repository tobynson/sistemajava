package Logica;

import Datos.vproducto;
import java.sql.*;//para no hacer muchas importaciones
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class fproducto {
    //instanciamos la clase conexion y accedo a su metodp conectar
    private conexion mysql = new conexion();
    private Connection cn =  mysql.Conectar();
    private String sql="";
    public Integer totalRegistros;
    
    //mostrar
    public DefaultTableModel mostrar(String buscar){
        DefaultTableModel modelo;
        
        String[]titulos={"Id","Nombre","Descripcion","Medida","Precio Venta"};
        String[]registros=new String[5];
        totalRegistros=0;
        
        modelo=new DefaultTableModel(null,titulos);
        sql="select * from producto where nombre like '%"+buscar+"%' order by idproducto";
        try {
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while (rs.next()) {      //de acuerdo al orden de la tabla          
                registros[0]=rs.getString("idproducto");
                registros[1]=rs.getString("nombre");
                registros[2]=rs.getString("descripcion");
                registros[3]=rs.getString("unidad_medida");
                registros[4]=rs.getString("precio_venta");
                                
                totalRegistros=totalRegistros+1;
                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return null;
        }
    }
    //insertar
    public boolean insertar(vproducto dts){
        sql="insert into producto(nombre,descripcion,unidad_medida,precio_venta) values(?,?,?,?)";
        try {//de acuerdo a los ?
            PreparedStatement pst=cn.prepareStatement(sql);
            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getDescripcion());
            pst.setString(3, dts.getUnidad_medida());
            pst.setDouble(4, dts.getPrecio_venta());
                      
            int n=pst.executeUpdate();
            if (n!=0) {
                return true;
            }else{
                return false;                
            }
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    //editar
    public boolean editar(vproducto dts){
        sql="update producto set nombre=?,descripcion=?,unidad_medida=?,precio_venta=? where idproducto=?";
        try {            
            PreparedStatement pst=cn.prepareStatement(sql);
            
            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getDescripcion());
            pst.setString(3, dts.getUnidad_medida());
            pst.setDouble(4, dts.getPrecio_venta());
            pst.setInt(5, dts.getIdproducto());//orden del signo en la sentencia sql
            
            int n=pst.executeUpdate();
            if (n!=0) {
                return true;
            }else{
                return false;
            }
            
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    //eliminar
    public boolean eliminar(vproducto dts){
        sql="delete from producto where idproducto=?";
        try {
            PreparedStatement pst=cn.prepareStatement(sql);
            pst.setInt(1, dts.getIdproducto());         
            int n=pst.executeUpdate();
            if (n!=0) {
                return true;
            }else{
                return false;
            }
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    
}
