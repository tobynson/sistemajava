package Logica;

import Datos.vcliente;
import Datos.vpersona;
import Datos.vtrabajador;
import java.sql.*;//para no hacer muchas importaciones
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ftrabajador {
     //instanciamos la clase conexion y accedo a su metodp conectar
    private conexion mysql = new conexion();
    private Connection cn =  mysql.Conectar();
    private String sql="";
    private String sql2="";
    public Integer totalRegistros;
    
    //mostrar
    public DefaultTableModel mostrar(String buscar){
        DefaultTableModel modelo;
        
        String[]titulos={"Id","Nombre","Apaterno","Amaterno","Tipo","Numero","Direccion","Telefono","Email","Sueldo","Acceso","Login","Password","Estado"};
        String[]registros=new String[14];
        totalRegistros=0;
        
        modelo=new DefaultTableModel(null,titulos);
        sql="select p.idpersona,p.nombre,p.apaterno,p.amaterno,p.tipo_documento,p.num_documento,"+
                "p.direccion,p.telefono,p.email,t.sueldo,t.acceso,t.login,t.password,t.estado "+
                "from persona p inner join trabajador t on  p.idpersona=t.idpersona where num_documento like '%"+buscar+"%' order by idpersona";
        try {
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while (rs.next()) {      //de acuerdo al orden de la tabla          
                registros[0]=rs.getString("idpersona");
                registros[1]=rs.getString("nombre");
                registros[2]=rs.getString("apaterno");
                registros[3]=rs.getString("amaterno");
                registros[4]=rs.getString("tipo_documento");
                registros[5]=rs.getString("num_documento");
                registros[6]=rs.getString("direccion");
                registros[7]=rs.getString("telefono");
                registros[8]=rs.getString("email");
                registros[9]=rs.getString("sueldo");
                registros[10]=rs.getString("acceso");
                registros[11]=rs.getString("login");
                registros[12]=rs.getString("password");
                registros[13]=rs.getString("estado");
                                
                totalRegistros=totalRegistros+1;
                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return null;
        }
    }
    //insertar
    public boolean insertar(vtrabajador dts){
        sql="insert into persona(nombre,apaterno,amaterno,tipo_documento,num_documento,direccion,telefono,email) values(?,?,?,?,?,?,?,?)";
        sql2="insert into trabajador(idpersona,sueldo,acceso,login,password,estado) values((select idpersona from persona order by idpersona desc limit 1),?,?,?,?,?)";
        try {//de acuerdo a los ?
            PreparedStatement pst=cn.prepareStatement(sql);            
            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getAmaterno());
            pst.setString(3, dts.getApaterno());
            pst.setString(4, dts.getTipo_documento());
            pst.setString(5, dts.getNum_documento());
            pst.setString(6, dts.getDireccion());
            pst.setString(7, dts.getTelefono());
            pst.setString(8, dts.getEmail());
            
            PreparedStatement pst2=cn.prepareStatement(sql2);            
            pst2.setDouble(1, dts.getSueldo());
            pst2.setString(2, dts.getAcceso());
            pst2.setString(3, dts.getLogin());
            pst2.setString(4, dts.getPassword());
            pst2.setString(5, dts.getEstado());
            
            int n=pst.executeUpdate();
            if (n!=0) {//si puedo insertar una persona inserto un cliente
                int n2=pst2.executeUpdate();
                if (n2!=0) {
                   return true; 
                }else{
                    return false;
                }
            }else{
                return false;                
            }
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    //editar
    public boolean editar(vtrabajador dts){
        sql="update persona set nombre=?,apaterno=?,amaterno=?,tipo_documento=?,num_documento=?,direccion=?,telefono=?,email=? where idpersona=?";
        sql2="update trabajador set sueldo=?,acceso=?,login=?,password=?,estado=? where idpersona=?";
       
        try {//de acuerdo a los ?
            PreparedStatement pst=cn.prepareStatement(sql);            
            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getAmaterno());
            pst.setString(3, dts.getApaterno());
            pst.setString(4, dts.getTipo_documento());
            pst.setString(5, dts.getNum_documento());
            pst.setString(6, dts.getDireccion());
            pst.setString(7, dts.getTelefono());
            pst.setString(8, dts.getEmail());
            pst.setInt(9, dts.getIdpersona());
            
            PreparedStatement pst2=cn.prepareStatement(sql2);            
            pst2.setDouble(1, dts.getSueldo());
            pst2.setString(2, dts.getAcceso());
            pst2.setString(3, dts.getLogin());
            pst2.setString(4, dts.getPassword());
            pst2.setString(5, dts.getEstado());
            pst2.setInt(6, dts.getIdpersona());
                      
            int n=pst.executeUpdate();
            if (n!=0) {//si puedo insertar una persona inserto un cliente
                int n2=pst2.executeUpdate();
                if (n2!=0) {
                   return true; 
                }else{
                    return false;
                }
            }else{
                return false;                
            }                   
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    //eliminar
    public boolean eliminar(vtrabajador dts){
        sql="delete from trabajador where idpersona=?";//primero la foranea
        sql2="delete from persona where idpersona=?";
        
        try {//de acuerdo a los ?
            PreparedStatement pst=cn.prepareStatement(sql);                   
            pst.setInt(1, dts.getIdpersona());
            
            PreparedStatement pst2=cn.prepareStatement(sql2);                        
            pst2.setInt(1, dts.getIdpersona());
                      
            int n=pst.executeUpdate();
            if (n!=0) {//si puedo insertar una persona inserto un cliente
                int n2=pst2.executeUpdate();
                if (n2!=0) {
                   return true; 
                }else{
                    return false;
                }
            }else{
                return false;                
            }                   
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    //login
    public DefaultTableModel login(String login,String password){
        DefaultTableModel modelo;
        
        String[]titulos={"Id","Nombre","Apaterno","Amaterno","Acceso","Login","Password","Estado"};
        String[]registros=new String[8];
        totalRegistros=0;
        
        modelo=new DefaultTableModel(null,titulos);
        sql="select p.idpersona,p.nombre,p.apaterno,p.amaterno,t.acceso,t.login,t.password,t.estado "+
                "from persona p inner join trabajador t on  p.idpersona=t.idpersona where t.login='"+login+"' and t.password='"+password+"' and t.estado='Activado'";
        try {
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while (rs.next()) {      //de acuerdo al orden de la tabla          
                registros[0]=rs.getString("idpersona");
                registros[1]=rs.getString("nombre");
                registros[2]=rs.getString("apaterno");
                registros[3]=rs.getString("amaterno");
                
                registros[4]=rs.getString("acceso");
                registros[5]=rs.getString("login");
                registros[6]=rs.getString("password");
                registros[7]=rs.getString("estado");
                                
                totalRegistros=totalRegistros+1;
                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return null;
        }
    }
}
