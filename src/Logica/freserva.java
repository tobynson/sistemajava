package Logica;

import Datos.vreserva;
import java.sql.*;//para no hacer muchas importaciones
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class freserva {
    //instanciamos la clase conexion y accedo a su metodp conectar
    private conexion mysql = new conexion();
    private Connection cn =  mysql.Conectar();
    private String sql="";
    public Integer totalRegistros;
    
    //mostrar
    public DefaultTableModel mostrar(String buscar){
        DefaultTableModel modelo;
        
        String[]titulos={"Id","Idhabitacion","Numero","Idcliente","Cliente","Idtrabajador","Trabajador","Tipo_reserva",
                         "Fecha_reserva","Fecha_ingreso","Fecha_salida","Costo_total","Estado"};
        String[]registros=new String[13];
        totalRegistros=0;
        
        modelo=new DefaultTableModel(null,titulos);
        sql="select r.idreserva,r.idhabitacion,h.numero,r.idcliente,"+
                "(select nombre from persona where idpersona=r.idcliente )as clienten,"+
                "(select apaterno from persona where idpersona=r.idcliente )as clienteap,"+
                    " r.idtrabajador,(select nombre from persona where idpersona=r.idtrabajador)as trabajadorn,"+
                "(select apaterno from persona where idpersona=r.idtrabajador) as trabajadorap,"+
                "r.tipo_reserva,r.fecha_reserva,r.fecha_ingreso,r.fecha_salidad,r.costo_total,r.estado "+
                " from reserva r inner join habitacion h on r.idreserva=h.idhabitacion where r.fecha_reserva like '%"+buscar+"%' order by idreserva desc";
        try {
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while (rs.next()) {      //de acuerdo al orden de la tabla          
                registros[0]=rs.getString("idreserva");
                registros[1]=rs.getString("idhabitacion");
                registros[2]=rs.getString("numero");
                registros[3]=rs.getString("idcliente");
                registros[4]=rs.getString("clienten")+" "+rs.getString("clienteap");
                registros[5]=rs.getString("idtrabajador");
                registros[6]=rs.getString("trabajadorn")+" "+rs.getString("trabajadorap");
                registros[7]=rs.getString("tipo_reserva");
                registros[8]=rs.getString("fecha_reserva");
                registros[9]=rs.getString("fecha_ingreso");
                registros[10]=rs.getString("fecha_salidad");
                registros[11]=rs.getString("costo_total");
                registros[12]=rs.getString("estado");
                
                                
                totalRegistros=totalRegistros+1;
                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return null;
        }
    }
    //insertar
    public boolean insertar(vreserva dts){
        sql="insert into reserva(idhabitacion,idcliente,idtrabajador,tipo_reserva,fecha_reserva,fecha_ingreso,fecha_salidad,costo_total,estado) values(?,?,?,?,?,?,?,?,?)";
        try {//de acuerdo a los ?
            PreparedStatement pst=cn.prepareStatement(sql);
            pst.setInt(1, dts.getIdhabitacion());
            pst.setInt(2, dts.getIdcliente());
            pst.setInt(3, dts.getIdtrabajador());
            pst.setString(4, dts.getTipo_reserva());
            pst.setDate(5, (Date)dts.getFecha_reserva());
            pst.setDate(6, (Date)dts.getFecha_ingreso()); 
            pst.setDate(7, (Date)dts.getFecha_salidad());
            pst.setDouble(8,dts.getCosto_total());
            pst.setString(9, dts.getEstado());
            
            int n=pst.executeUpdate();
            if (n!=0) {
                return true;
            }else{
                return false;                
            }
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    //editar
    public boolean editar(vreserva dts){
        sql="update reserva set idhabitacion=?,idcliente=?,idtrabajador=?,tipo_reserva=?,fecha_reserva=?,fecha_ingreso=?,fecha_salidad=?,costo_total=?,estado=? where idreserva=?";
        try {            
            PreparedStatement pst=cn.prepareStatement(sql);
            
            pst.setInt(1, dts.getIdhabitacion());
            pst.setInt(2, dts.getIdcliente());
            pst.setInt(3, dts.getIdtrabajador());
            pst.setString(4, dts.getTipo_reserva());
            pst.setDate(5, (Date)dts.getFecha_reserva());
            pst.setDate(6, (Date)dts.getFecha_ingreso()); 
            pst.setDate(7, (Date)dts.getFecha_salidad());
            pst.setDouble(8,dts.getCosto_total());
            pst.setString(9, dts.getEstado());
            pst.setInt(10, dts.getIdreserva());//orden del signo en la sentencia sql
            
            int n=pst.executeUpdate();
            if (n!=0) {
                return true;
            }else{
                return false;
            }
            
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    //eliminar
    public boolean eliminar(vreserva dts){
        sql="delete from reserva where idreserva=?";
        try {
            PreparedStatement pst=cn.prepareStatement(sql);
            pst.setInt(1, dts.getIdreserva());         
            int n=pst.executeUpdate();
            if (n!=0) {
                return true;
            }else{
                return false;
            }
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null,e);
            return false;
        }
    }
    
}
